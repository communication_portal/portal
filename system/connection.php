<?php
			require_once 'config/sqlDefinitions.php';
			require_once 'system/PdoDebugger.php';
//			$db = mysql_connect(MYSQL_Server, MYSQL_User,MYSQL_Password) or die(mysql_error($db));
//			mysql_select_db(DATABASE, $db) or die(mysql_error($db));

			class Connection
			{
				private static $instance = NULL;
				public static $db = NULL;
				

				private function __construct()
				{
					
					$this->db = new PDO('mysql:host='.MYSQL_Server.';dbname='.DATABASE.';charset=utf8',MYSQL_User , MYSQL_Password);

				}

				public function DebugQuery($sql, $params = false)
				{
					echo PdoDebugger::show($sql, $params);
					die;
				}
				
				
				
				public static function GetInstance()
				{
					if(!self::$instance)
					{

						self::$instance = new Connection();
					}
					return self::$instance;
				}
				
				public function Query($query,$params=array())
				{

					try {
						    //connect as appropriate as above
						    // $result = $this->db->query($query); //invalid query!
						$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
						
							$stmt = $this->db->prepare($query);
							$stmt->execute(($params));

							$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
							if(empty($rows))
								{
									return false;
								}
							else 
								{

									return $rows;
								}
						} catch(PDOException $ex) {
						    echo "An Error occured!"; //user friendly message
						    var_dump($ex->getMessage()); die;
						}
				
				}
				
				public function InsertQuery($query, $params = array())
				{
					try {
						    //connect as appropriate as above
						$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
						    // $result = $this->db->query($query); //invalid query!
							$stmt = $this->db->prepare($query);
							$stmt->execute($params);
							$affected_rows = $stmt->rowCount();
							if($affected_rows > 0) return $this->db->lastInsertId();
							else return false;
						} catch(PDOException $ex) {
						    echo "An Error occured!"; //user friendly message
						    var_dump($ex->getMessage()); die;
						}
				}

				public function UpdateQuery($query, $params = array())
				{
					try {
						    //connect as appropriate as above
						    // $result = $this->db->query($query); //invalid query!
						$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
							$stmt = $this->db->prepare($query);
							$stmt->execute($params);
							$affected_rows = $stmt->rowCount();
							if($affected_rows > 0) return true;
							else return false;
						} catch(PDOException $ex) {
						    echo "An Error occured!"; //user friendly message
						    var_dump($ex->getMessage()); die;
						}
				}
				
				public function CreateQuery($query)
				{
					// mysql_query($query, self::$db);
					// return true;
				}
				
				public function DeleteQuery($query,$params = array())
				{
					try {
						    //connect as appropriate as above
						    // $result = $this->db->query($query); //invalid query!
						$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
							$stmt = $this->db->prepare($query);
							$stmt->execute($params);
							$affected_rows = $stmt->rowCount();
							if($affected_rows > 0) return true;
							else return false;
						} catch(PDOException $ex) {
						    echo "An Error occured!"; //user friendly message
						    var_dump($ex->getMessage()); die;
						}
				}
				
				public static function GetRowCount($query)
				{
					// $result = mysql_query($query);
					// $rows = mysql_num_rows($result);
					// return $rows;
				}
				
				public static function GetRecord($result)
				{
					// $row = mysql_fetch_row($result);
					// return $row;
				}
				public static function GetInsertID()
				{
					try
					{
						$stmt = self::GetInstance()->db->lastInsertId();
						return $stmt;
					}	
					catch(PDOException $ex)
					{
						echo "An Error occured!"; //user friendly message
						var_dump($ex->getMessage()); die;
					}
				}
			}

?>