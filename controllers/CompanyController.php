<?php
/**
* 
*/
class CompanyController extends Controller
{

	
	public function __construct()
	{
		parent::__construct();

		loadHelper('url');
		$session = Session::getCurrentSession();
		if(!$session)
		{
			redirect('login');
		}
	}

	public function getListAction($company_id)
	{
		$data['designations'] = getModel('designation')->getCollection($company_id);
		$this->view->renderWithoutAnything('designation/select.phtml',$data);
	}
	
}
?>