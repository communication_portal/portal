<?php
class WarehouseController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		loadHelper('url');
		$session = Session::getCurrentSession();
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$data['warehouses'] = getModel('warehouse')->getCollection();
		$this->view->render('warehouse/warehouse.phtml',$data);
	}

	public function addWarehouseAction()
	{
		$data['users'] = getModel('users')->getCollection();
		$this->view->render('warehouse/form.phtml',$data);

	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$warehouse = getModel('warehouse')->insert($data);
		if($warehouse == CONFLICT)
		{
			Session::AddErrorMessage('Warehouse already exists');
			redirect('warehouse/addWarehouse');
		}
		else
		{
			Session::AddSuccessMessage('Warehouse successfully added.');

			redirect('warehouse');
		}

	}

	public function editWarehouseAction($warehouse_id)
	{
		loadHelper('inputs');
		$data['warehouse'] = getModel('warehouse')->load($warehouse_id);
		$data['users'] = getModel('users')->getCollection();
		$this->view->render('warehouse/edit-form.phtml',$data);


	}

	public function editAction($warehouse_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['warehouse_id'] = $warehouse_id;
		getModel('warehouse')->edit($data);
		Session::AddSuccessMessage('Warehouse successfully edited.');

redirectToPrevPage();


	}

	public function viewAction($warehouse_id)
	{
		loadHelper('inputs');
		$data['warehouse'] = getModel('warehouse')->load($warehouse_id);
		$data['items'] = getModel('items')->getItemsWarehouse($warehouse_id);
		$this->view->render('warehouse/view.phtml',$data);
	}

	public function deleteAction($warehouse_id)
	{
		loadHelper('inputs');
		getModel('warehouse')->delete($warehouse_id);
		Session::AddSuccessMessage('Warehouse successfully deleted.');


		redirect('warehouse');

	}

}
?>