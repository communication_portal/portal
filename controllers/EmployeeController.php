<?php
class EmployeeController extends Controller
{
	public function __construct()
	{
		loadHelper('url');
		parent::__construct();
		$session = Session::getCurrentSession();
		if(!$session)
		{
			redirect('login');
		}
	}
	
	public function indexAction()
	{
		$data['employee'] = getModel('employee')->GetCollection();
		$this->view->render('users/users.phtml',$data);
	}

	public function profileAction($id)
	{
		$data['employee'] = getModel('employee')->getInfoById($id);
		$this->view->render('employee/profile.phtml',$data);
	}

	public function addUsersAction()
	{
		$data['departments'] = getModel('departments')->getCollection();
		$this->view->render('users/form.phtml',$data);
	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$user_id = getModel('users')->insert($data);
		if(!$user_id)
		{
			Session::AddErrorMessage('Username or email already exists');
			redirect('users/addUsers');

		}
		elseif($user_id == ERROR)
		{
			Session::AddErrorMessage('Username cannot be admin');
			// $data['error_msg'] = "Admin cannot be user";
			redirect('users/addUsers');
		}
		else
		{

			if($this->sendEmail($data['user_email']))
			{
				Session::AddSuccessMessage('User successfully added. An email has been sent for confirmation.');
				redirect('users');

			}
			else
			{
				Session::AddSuccessMessage('User successfully added.');
				Session::AddErrorMessage('Failed to send email');
				redirect('users');
			}
		}


		
	}

	private function sendEmail($email)
	{
		$to = ltrim($email); // this is your Email address
	    $from = "rashmi@manaram.technology.com"; // this is the sender's Email address
	    $subject = "Set your password";
	    $html = "<a href=" .URL."password/setPassword/". md5($email).">Set Password</a>";
	    $message = "Goto the following link to create your password \n" . $html;


	    if(mail($to,$subject,$message, $from))
	    {
	    	$success_msg = "The mail was successfully sent";
	    	$_POST = array();
	    	return true;
	    }
	    else
	    {

	    	$error_msg = "email was NOT sent";
	    	$_POST = array();
	    	return false;
	    }

	}

	public function editUsersAction($user_id)
	{
		loadHelper('inputs');
		$data['departments'] = getModel('departments')->getCollection();
		$data['user'] = getModel('users')->load($user_id);
		//$data['error_msg'] = (isset($error_msg))? $error_msg : null;
		$this->view->render('users/edit-form.phtml',$data);
	}

	public function editAction($user_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['user_id'] = $user_id;
		$user = getModel('users')->edit($data);
		var_dump($user);
		if($user == NOCONFLICT)
		{
			Session::AddSuccessMessage('User edited');
			redirectToPrevPage();

		}

		else if($user == CONFLICT)
		{
			Session::AddErrorMessage('Username or email already exists');
			$this->editUsersAction($user_id);

		}
		else if($user == ERROR)
		{
			Session::AddErrorMessage('Username cannot be admin');
			// $data['error_msg'] = "Admin cannot be user";
			$this->editUsersAction($user_id);
		}
	}

	public function deleteAction($user_id)
	{
		loadHelper('inputs');
		getModel('users')->delete($user_id);

		redirect('users');
	}

	public function viewAction($user_id)
	{
		loadHelper('inputs');
		$data['user'] = getModel('users')->load($user_id);
		$data['departments'] = getModel('users')->loadDepartmentByUser($user_id);
		$this->view->render('users/view.phtml',$data);
	}

}
?>