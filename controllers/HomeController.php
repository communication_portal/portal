<?php
class HomeController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
		//getModel('home')->test();
		$this->view->render('home/home.phtml');
	}
}