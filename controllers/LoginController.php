<?php
class LoginController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
		// getModel('home')->test();
		loadhelper('inputs');
		loadhelper('URL');
		$session = Session::getCurrentSession();

		if($session)
		{
			redirect('employee/profile');
		}
		
		$this->view->render('login/login.phtml',false, false, false);
	}

	public function loginpostAction()
	{
		loadhelper('inputs');
		loadhelper('URL');
		
		$rem_me = getPost('remember_me');
		$username = getPost('username');
		$password = md5(getPost('password'));
		if($username == "" || $password == "")
		{
			Session::addErrorMessage('Enter username and password');
			redirect('login');
		}

		else if(getModel('login')->authenticate($username,$password,$rem_me))
		{
			redirect('employee/profile');
		}
		else
		{
			Session::addErrorMessage('Login Unsuccessful');
			redirect('login');
		}
	}

	public function logoutAction()
	{
		loadhelper('inputs');
		loadhelper('URL');
		Session::session_close();
		redirect('login');
	}

	public function forgotAction($username = false)//do this at the end
	{
		if (!$username)
		{
			Session::addErrorMessage('Enter username');
			redirect('login');
		}
		else
		{
			$user_id = getModel('users')->loadByUsername($username);
			$result = getModel('login')->forgotPassword($user_id);
			if($result)
			{
				getModel('login')->sendEmail($username, $user_id);
			}
			Session::addSuccessMessage('Please check your mail to reset your password');
			redirect('login');
		}
	}

	public function newUserAction()
	{
		$data['companies'] = getModel('company')->GetCollection();
		$this->view->render('login/registerUser.phtml',$data,false,false,false);
	}

	public function registerUserAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$user_id = getModel('employee')->insertEmployee($data);
		if(!$user_id)
		{
			Session::AddErrorMessage('Username or email already exists');
			redirect('users/addUsers');

		}
		elseif($user_id == ERROR)
		{
			Session::AddErrorMessage('Username cannot be admin');
			// $data['error_msg'] = "Admin cannot be user";
			redirect('users/addUsers');
		}
		else
		{

			if($this->sendEmail($data['user_email']))
			{
				Session::AddSuccessMessage('User successfully added. An email has been sent for confirmation.');
				redirect('users');

			}
			else
			{
				Session::AddSuccessMessage('User successfully added.');
				Session::AddErrorMessage('Failed to send email');
				redirect('users');
			}
		}
	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$employee_id = getModel('employee')->insert($data);
		if(!$employee_id)
		{
			redirect('employee/register');
		}
		else
		{
			redirect('login');
		}
	}

}