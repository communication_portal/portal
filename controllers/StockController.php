<?php
class StockController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$session = Session::getCurrentSession();

		loadHelper('url');
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$data['items'] = getModel('items')->getCollection();
		$this->view->render('stock/stock.phtml',$data);
	}

	public function filterItemsAction()
	{
		loadHelper('inputs');
		$data = getPost();
		if(isset($data))
		{
			$data['item_info'] = getModel('items')->load($data['item'])[0];
			$data['departments'] = getModel('departments')->getDepartmentData($data['item']);
			$data['warehouses'] = getModel('warehouse')->getWarehouseData($data['item']);
			$html = $this->view->renderWithoutAnything('stock/table.phtml', $data);
		}
		echo $html;
	}

	public function purchaseAction()
    {
         loadHelper('inputs');
         $data = getPost();
         getModel('purchase')->purchase($data);
         redirect('purchase');
     }

}
?>