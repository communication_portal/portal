<?php

class NotificationController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        loadHelper('url');
        $session = Session::getCurrentSession();
        if(!$session)
        {
            redirect('login');
        }
    }

    public function indexAction()
    {
        $data['departments'] = getModel('departments')->getCollection();
        $data['items'] = getModel('items')->getCollection();
        $this->view->render('notification/notification.phtml', $data);
    }

    public function stockAction()
    {
        loadHelper('inputs');
        $data = getPost();
        if(isset($data['quantity']))
        {
            $lotNo = getModel('warehouse')->loadLotNo($data['item_id'], $data['warehouse_id'], $data["quantity"]);
            getModel('notification')->stock($data, $lotNo);
            redirect('notification');
        }
        else
        {
            $lotNo = getModel('warehouse')->loadLotNo($data['item_id'], $data['warehouse_id'], $data['stockQty']);
            getModel('notification')->stock($data, $lotNo);
            redirect('stock');
        }

    }

    public function purchaseAction()
    {
       loadHelper('inputs');
       $data = getPost();
       getModel('notification')->purchase($data);
       redirect('notification');
   }

   public function etaAction()
   {
       loadHelper('inputs');
       $data = getPost();
       getModel('notification')->delayETA($data);
       redirect ('notification');
   }



   public function getLotNoAction($notification_purchase_id)
   {
        $data['purchase_id'] = $notification_purchase_id;
        $data['warehouses'] = getModel('warehouse')->getCollection();
        $this->view->render('notification/lotno.phtml',$data);
    }

    public function ETAacceptAction()
    {
        loadHelper('inputs');
        $data = getPost();
        getModel('notification')->acceptETA($data);
        redirect('notification');
    }

    public function etarejectAction($purchaseId)
    {
        getModel('notification')->rejectETA($purchaseId);
        redirect('notification');
    }

    public function filterStockAction()
    {
        loadHelper('inputs');
        loadHelper('notification');
        $condition = getPost();
        if(isset($condition))
        {
          if ($condition == NULL || $condition['department'] == 0)
          {
            $data['notificationstock'] = notificationStock();
            $html = $this->view->renderWithoutAnything('notification/table_stock.phtml', $data);
          }
          else
          {
            $data['notificationstock'] = notificationStock($condition['department']);
            $html = $this->view->renderWithoutAnything('notification/table_stock.phtml', $data);
          }
        }
        echo $html;
    }

    public function filterPurchaseAction()
    {
        loadHelper('inputs');
        loadHelper('notification');
        $condition = getPost();
        if(isset($condition))
        {
          if ($condition == NULL || $condition['item'] == 0)
          {
            $data['notificationpurchase'] = notificationPurchase();
            $html = $this->view->renderWithoutAnything('notification/table_purchase.phtml', $data);
          }
          else
          {
            $data['notificationpurchase'] = notificationPurchase($condition['item']);
            $html = $this->view->renderWithoutAnything('notification/table_purchase.phtml', $data);
          }
        }
        echo $html;
    }

     public function filterETAAction()
    {
        loadHelper('inputs');
        loadHelper('notification');
        $condition = getPost();
        if(isset($condition))
        {
          if ($condition == NULL || $condition['item'] == 0)
          {
            $data['notificationeta'] = notificationETA();
            $html = $this->view->renderWithoutAnything('notification/table_eta.phtml', $data);
          }
          // else
          // {
          //   $data['notificationeta'] = notificationETA($condition['item']);
          //   $html = $this->view->renderWithoutAnything('notification/table_eta.phtml', $data);
          // }
        }
        echo $html;
    }
}