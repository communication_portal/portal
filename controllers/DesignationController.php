<?php 
class DesignationController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getListAction($company_id)
	{
		$data['designations'] = getModel('designation')->getCollection($company_id);
		$this->view->renderWithoutAnything('designation/select.phtml',$data);
	}
}