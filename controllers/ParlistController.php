<?php
class ParlistController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$session = Session::getCurrentSession();

		loadHelper('url');
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$data['item'] = getModel('items')->loadDeptItem();
		$data['items'] = getModel('items')->getCollection();
		$data['departments'] = getModel('departments')->getCollection();
		
		$this->view->render('parlist/parlist.phtml',$data);
	}

	public function editDeptValueAction($department_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['department_id'] = $department_id;
		getModel('parlist')->updateDeptItems($data);

		redirect('parlist');
	}

	public function filterItemsDeptAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$data['items'] = getModel('parlist')->getItemsDept($data['department']);
		$html = $this->view->renderWithoutAnything('parlist/table.phtml', $data);
		echo $html;

	}

}
?>