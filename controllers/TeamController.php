<?php
class TeamController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$session = Session::getCurrentSession();

		loadHelper('url');
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$session = Session::getCurrentSession();
		$company_id = getModel('employee')->getInfoById($session['user_id'])['company_id'];
		$data['employees'] = getModel('employee')->getCollectionByCompanyId($company_id);
		$data['session'] = $session;

		$this->view->render('groups/addGroup.phtml',$data);
	}

	public function viewAction($team_id)
	{
		$data['team'] = getModel('team')->loadTeam($team_id);
		$this->view->render('groups/view.phtml',$data);
	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$result = getModel('group')->insert($data);
		if($result == CONFLICT)
		{
			Session::AddErrorMessage('Team name already exists.Please choose another team name');
		}
		else
		{
			Session::AddSuccessMessage('Team successfully created.');
		}

		redirect('group');
	}

	public function editItemsAction($item_id)
	{
		$data['item'] = getModel('items')->load($item_id)[0];
		$data['departments'] = getModel('departments')->getCollection();
		$data['warehouses'] = getModel('warehouse')->getCollection();
		$data['vendors'] = getModel('vendors')->getCollection();
		$this->view->render('items/edit-form.phtml',$data);
	}

	public function editAction($item_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['item_id'] = $item_id;
		getModel('items')->editItems($data);
		Session::AddSuccessMessage('Item successfully edited.');
		redirectToPrevPage();

	}

	public function deleteAction($item_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['item_id'] = $item_id;
		getModel('items')->deleteItems($data);
		Session::AddSuccessMessage('Item successfully deleted.');
		redirect('items');
	}


	
}
?>