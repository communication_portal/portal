<?php
class LotnoController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$session = Session::getCurrentSession();

		loadHelper('url');
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$data['items'] = getModel('items')->getCollection();
		//$data['lotNos'] = getModel('lotno')->getCollection();
		$this->view->render('lotno/lotno.phtml',$data);
	}

	public function filterItemsAction()
	{
		loadHelper('inputs');
		$data = getPost();
		//var_dump($data);
		if(isset($data))
		{
			$data['lotnos'] = getModel('lotno')->load($data['item']);
			$html = $this->view->renderWithoutAnything('lotno/table.phtml', $data);
		}
		echo $html;
	}

	public function saveAction()
	{
		loadHelper('inputs');
		$data = getPost();
		foreach($data['quantity'] as $key => $value) {
			$lotno = "$key";
			$quantity = "$value";
			getModel('lotno')->save($lotno, $quantity);
		}
		getModel('items')->updateItemQty($data['item'],$data['warehouse']);

		redirect('lotno');
	}

}
?>