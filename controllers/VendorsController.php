<?php
class VendorsController extends Controller
{
	public function __construct()
	{
		loadHelper('url');
		parent::__construct();
	}

	public function indexAction()
	{
		$data['vendors'] = getModel('vendors')->getCollection();
		$data['items'] = getModel('items')->getCollection();
		$this->view->render('vendors/vendors.phtml',$data);
	}

	public function getVendorInfoAction()
	{
		loadHelper('inputs');
		$condition = getPost();
		$data['vendor'] = getModel('vendors')->getVendorInfo($condition['vendor']);
		$html = '<label>Lead Time: '. $data['vendor'][0]['lead_time'].'</label><br> <label> Vendor Note:'.$data['vendor'][0]['vendor_note'].'</label>';
		echo $html;
	}

	public function filterItemsAction()
	{
		loadHelper('inputs');
		$condition = getPost();
		if(isset($condition))
		{
			if ($condition == NULL || ($condition['items'] == 0))
			{
			$data['vendors'] = getModel('vendors')->getCollection();
				$html = $this->view->render('vendors/table.phtml', $data,false,false);
			}
			else
			{
				$data['vendors'] = getModel('vendors')->getVendors($condition['items']);
				$html = $this->view->render('vendors/table.phtml', $data, false,false);
			}
		}
		echo $html;
	}


	public function addVendorItemsAction()
	{
		loadHelper('inputs');
		//die('here');
		//echo URL."vendors/addVendor";
	}

	public function addVendorAction()
	{
		$this->view->render('vendors/form.phtml');
	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$data['from_items'] = $_POST['from_items'];
		$data['vendorType'] = $_POST['vendorType'];

		$vendor_id = getModel('vendors')->insertVendors($data);

		if($data['vendorType'] == "1")
		{
			$_SESSION['items']['primary_vendor'] = $vendor_id;
		}
		else
		{
			$_SESSION['items']['secondary_vendor'] = $vendor_id;
		}
		if($data["from_items"] == "1")
		{
			if($_POST['edit'] == "0") 
			{
				if($vendor_id == CONFLICT)
				{
					Session::AddErrorMessage('Vendor already exists');
				}
				redirect('items/addItems');
			}
			else
			{
				if($vendor_id == CONFLICT)
				{
					Session::AddErrorMessage('Vendor already exists');
				}
				redirect('items/editItems/'. $_SESSION['items']['item_id']);
			}
		}
		else
		{
			if($vendor_id == CONFLICT)
			{
				Session::AddErrorMessage('Vendor already exists');
				redirect('vendors/addVendor');
			}
			else
			{
				Session::AddSuccessMessage('Vendor successfully added.');
				redirect('vendors');
			}
		}
		
	}

	public function editVendorAction($vendor_id)
	{
		$data['vendor'] = getModel('vendors')->load($vendor_id)[0];
		$this->view->render('vendors/edit-form.phtml',$data);

	}

	public function editAction($vendor_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['vendor_id'] = $vendor_id;
		$vendor = getModel('vendors')->editVendor($data);
		if(!$vendor)
		{
			Session::AddErrorMessage('Vendor already exists');
			$this->editVendorAction($vendor_id);
		}
		else
		{
			Session::AddSuccessMessage('Vendor successfully edited.');
			redirectToPrevPage();

		}
	}

	public function deleteAction($vendor_id)
	{
		loadHelper('inputs');
		getModel('vendors')->deleteVendor($vendor_id);
		Session::AddSuccessMessage('Vendor successfully deleted.');
		redirect('vendors');
	}

	public function viewAction($vendor_id)
	{
		loadHelper('inputs');
		$data['vendors'] = getModel('vendors')->viewVendor($vendor_id);
		$data['items'] = getModel('items')->getItemVendor($vendor_id);
		$this->view->render('vendors/view.phtml', $data);
	}


}
?>

