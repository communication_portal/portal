<?php
class DepartmentsController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		loadHelper('url');
		$session = Session::getCurrentSession();
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$session = Session::getCurrentSession();
		if($session['user_access_level'] == INVENTORY_MANAGER || $session['user_access_level'] == PURCHASE_MANAGER )
		{
			$data['departments'] = getModel('departments')->getCollection();
			$this->view->render('departments/departments.phtml',$data);
		}
		else 
		{
			$data['departments'] = getModel('departments')->loadByUser($session["user_id"]);
			$this->view->render('departments/department.phtml',$data);
		}
	}

	public function dailyUsageAction()
	{
		$data['departments'] = getModel('departments')->getCollection();
		$checkdata = getModel('departments')->checkUsageDepts();
		if($checkdata)
			Session::AddSuccessMessage('Daily Usage has been updated for all departments');

		$this->view->render('departments/dailyUsage.phtml',$data);
	}

	public function usageAction()
	{
		loadHelper('inputs');
		$data = getPost();
		//var_dump($data);die;
		
		$this->array2str($data['usage'],$data);

		$session = Session::getCurrentSession();
		if($session['user_access_level'] == 1)
		{
			$this->dailyUsageAction();
		}
		else
		{
			redirect('dashboard');
		}

	}

	function array2str($array,$data)
	{
		if(!isset($data['date']))
		{
			$data['date'] = date('Y-m-d');
		}
		$out = "";
		$usage[][] ="";
		if(getModel('departments')->checkUsage($data['department_id']))
		{
			$usage = getModel('departments')->deleteUsage($data['department_id'], $data['date']);
		}
		foreach($array as $key => $value) {
			$item['item_id'] = "$key";
			$item['daily_usage'] = "$value";
			if (!isset($usage[$data['department_id']][$item['item_id']]))
			{
				$usage[$data['department_id']][$item['item_id']] = "";
			}

			getModel('departments')->insertUsage($item,$data['department_id'],$data['date_usage'], $usage[$data['department_id']][$item['item_id']]);
		}
	}

	public function viewAction($department_id)
	{
		loadHelper('inputs');
		$data['department'] = getModel('departments')->load($department_id);
		$data['items'] = getModel('items')->getItems($department_id);
		$this->view->render('departments/view.phtml',$data);
	}

	public function addDepartmentAction()
	{
		$data['users'] = getModel('users')->getCollection();
		$this->view->render('departments/form.phtml',$data);
	}

	public function addAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$department_id = getModel('departments')->insert($data);
		Session::AddSuccessMessage('Department successfully added.');
		redirect('departments');

	}

	public function editDepartmentAction($department_id)
	{
		loadHelper('inputs');
		$data['department'] = getModel('departments')->load($department_id);
		$data['users'] = getModel('users')->getCollection();
		$this->view->render('departments/edit-form.phtml',$data);
	}

	public function editAction($department_id)
	{
		loadHelper('inputs');
		$data = getPost();
		// var_dump($data);die;
		$data['department_id'] = $department_id;
		getModel('departments')->edit($data);
		Session::AddSuccessMessage('Department successfully edited.');

		redirectToPrevPage();


	}

	public function deleteAction($department_id)
	{
		loadHelper('inputs');
		getModel('departments')->delete($department_id);
		Session::AddSuccessMessage('Department successfully deleted.');

		redirect('departments');

	}
}
?>