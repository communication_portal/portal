<?php
/**
* 
*/
class ReportController extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		loadHelper('url');
		$session = Session::getCurrentSession();
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		//$data['warehouses'] = getModel('warehouse')->getCollection();
		$this->view->render('reports/form.phtml');
	}

	public function generateReportAction()
	{
		loadHelper('inputs');
		$data = getPost();
		if($data['category'] == 1)
		{
			$data['report'] = getModel('report')->loadDept($data);
			$this->view->render('reports/reportDept.phtml', $data);

		}
		else
		{
			$data['report'] = getModel('report')->loadItem($data);
			$this->view->render('reports/reportItem.phtml', $data);

		}

	}
}
?>