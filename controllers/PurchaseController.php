<?php
class PurchaseController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$session = Session::getCurrentSession();

		loadHelper('url');
		if(!$session)
		{
			redirect('login');
		}
	}

	public function indexAction()
	{
		$data['items'] = getModel('items')->getCollection();
		$this->view->render('purchase/purchase.phtml',$data);
	}

	public function filterItemsAction()
	{
		loadHelper('inputs');
		$data = getPost();
		if(isset($data))
		{
			$data['item_info'] = getModel('items')->load($data['item'])[0];
			$data['vendors'] = getModel('vendors')->getVendors($data['item']);
			$data['departments'] = getModel('departments')->getDepartmentData($data['item']);
			$data['warehouses'] = getModel('warehouse')->getWarehouseData($data['item']);
			$data['alternate_vendors'] = getModel('vendors')->getCollection();
			$html = $this->view->renderWithoutAnything('purchase/table.phtml', $data);
		}
		echo $html;
	}

	public function filterItemsDeptAction()
	{
		loadHelper('inputs');
		$data = getPost();
		$data['items'] = getModel('parlist')->getItemsDept($data['department']);
		$html = $this->view->renderWithoutAnything('parlist/table.phtml', $data);
		echo $html;

	}

	public function purchaseAction()
    {
         loadHelper('inputs');
         $data = getPost();
         getModel('purchase')->purchase($data);
         redirect('purchase');
     }

}
?>