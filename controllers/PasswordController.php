<?php
class PasswordController extends Controller
{
	public function __construct()
	{
		loadHelper('url');
		parent::__construct();
	}
	public function setPasswordAction($token_id)
	{
		loadHelper('inputs');
		$data['token_id'] = $token_id;
		if(getModel('users')->checkSetPassword($token_id))
		{
			$this->view->render('users/set-password.phtml',$data);
		}
		else
		{
			$error_msg = "Password has already been set";
		}
	}

	public function addPasswordAction($token_id)
	{
		loadHelper('inputs');
		$data = getPost();
		$data['token_id'] = $token_id;
		$user_id = getModel('users')->editUserPassword($data);

		redirect('login');
	}


}
?>
