<?php
class PagenotfoundController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
		$this->view->render('default/404.phtml');
	}
}