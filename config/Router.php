<?php
require_once 'system/session.php';
require_once 'system/adminsession.php';
class route
{
	public $routes =	array();
	/**
	 * Current class name
	 *
	 * @var	string
	 */
	public $class =		'';

	/**
	 * Current method name
	 *
	 * @var	string
	 */
	public $method =	'index';

	public $url = array();

	public function __construct()
	{
		// echo $this->uri(2);
		// die;
		
		$this->_set_routing();
	}

	protected function _set_routing()
	{
		$default = DEFAULT_CONTROLLER . '/' . DEFAULT_ACTION;
		$url = isset($_GET['url']) ? $_GET['url'] : $default;
		$url = rtrim($url, '/');
		$url = explode('/', $url);
		$this->url = $url;

		if($url[0] == ADMIN_FOLDER_NAME)
		{
			$this->_set_admin_controller($url);
		}
		if (file_exists('config/routes.php'))
		{
			include('config/routes.php');
		}
		$this->routes = $route;
		
		$this->_parse_routes($url);
	}

	
/**
	 * Set class name
	 *
	 * @param	string	$class	Class name
	 * @return	void
	 */
	public function set_class($class)
	{
		$this->class = str_replace(array('/', '.'), '', $class);
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch the current class
	 *
	 * @deprecated	3.0.0	Read the 'class' property instead
	 * @return	string
	 */
	public function fetch_class()
	{
		return $this->class;
	}

	// --------------------------------------------------------------------

	/**
	 * Set method name
	 *
	 * @param	string	$method	Method name
	 * @return	void
	 */
	public function set_method($method)
	{
		$this->method = $method;
	}

	// --------------------------------------------------------------------

	/**
	 * Fetch the current method
	 *
	 * @deprecated	3.0.0	Read the 'method' property instead
	 * @return	string
	 */
	public function fetch_method()
	{
		return $this->method;
	}

	protected function _parse_routes($url)
	{
		$uri = implode('/', $url);
		$http_verb = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : 'cli';
		foreach ($this->routes as $key => $val)
		{
			if (is_array($val))
			{
				$val = array_change_key_case($val, CASE_LOWER);
				if (isset($val[$http_verb]))
				{
					$val = $val[$http_verb];
				}
				else
				{
					continue;
				}
			}
			// Convert wildcards to RegEx
			$key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
			// Does the RegEx match?
			if (preg_match('#^'.$key.'$#', $uri, $matches))
			{
				// Are we using callbacks to process back-references?
				if ( ! is_string($val) && is_callable($val))
				{
					// Remove the original string from the matches array.
					array_shift($matches);

					// Execute the callback using the values in matches as its parameters.
					$val = call_user_func_array($val, $matches);
				}
				// Are we using the default routing method for back-references?
				elseif (strpos($val, '$') !== FALSE && strpos($key, '(') !== FALSE)
				{
					$val = preg_replace('#^'.$key.'$#', $val, $uri);
				}

				$this->_set_request(explode('/', $val));
				return;
			}
			else
			{
				$this->_set_request();
			}
		// var_dump(isset($_SERVER['REQUEST_METHOD']));
		}
	}

	protected function _set_request($segments = array())
	{
		if (empty($segments))
		{
			$this->_set_default_controller();
			return;
		}

		$this->set_class($segments[0]);
		if (isset($segments[1]))
		{
			$this->set_method($segments[1]);
		}
		else
		{
			$segments[1] = 'index';
		}
		array_unshift($segments, NULL);
		unset($segments[0]);
		$control = $this->class . 'Controller';
		$controller = new $control();
		$function = $this->method . 'Action';
		if (isset($segments[3]))
			$controller->$function($segments[3]);
		else
			$controller->function();
	}


	protected function _set_admin_controller()
	{
		$url = $this->url;
		if(isset($url[1]))
		{
			$control = $url[1] . 'Controller';
		}
		else
		{
			$default_cont = DEFAULT_ADMIN_CONTROLLER . 'Controller';
			$controller = new $default_cont();
			$default_act = DEFAULT_ADMIN_ACTION . 'Action';
		}
		if (!class_exists($control)) 
		{
			include('views/default/404.phtml');
		}
		else
		{
			$controller = new $control();
			$default_act = 'indexAction';
			$function = isset($url[2])?$url[2] . 'Action':'';
			if (!method_exists($controller, $function)) 
			{
				if(!method_exists($controller, $default_act))
					include('views/default/404.phtml');
				else
					$controller->$default_act();
			}
			else
			{
				if(isset($url[3]))
					$controller->$function($url[3]);
				elseif(isset($url[2]))
					$controller->$function();
			}
		}
	}

	protected function _set_default_controller()
	{
		$url = $this->url;
		if(isset($url[0])){
			$control = $url[0] . 'Controller';
		}
		else
		{
			$default_cont = DEFAULT_CONTROLLER . 'Controller';
			$controller = new $default_cont();
			$default_act = DEFAULT_ACTION . 'Action';
		}
		if (!class_exists($control)) 
		{
			include('views/default/404.phtml');
		}
		else
		{
			$controller = new $control();
			$default_act = 'indexAction';
			$function = isset($url[1])?$url[1] . 'Action': '';
			if (!method_exists($controller, $function)) 
			{
				if(!method_exists($controller, $default_act))
					include('views/default/404.phtml');
				else
					$controller->$default_act();
			}
			else
			{
				if(isset($url[2]))
					$controller->$function($url[2]);
				elseif(isset($url[1]))
					$controller->$function();
			}
		}
	}
}
?>