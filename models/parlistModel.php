<?php
class ParlistModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function insertDeptItems($data)
	{
		foreach ($data['item'] as $item_id => $value) {
			$sql = "DELETE FROM department_item WHERE item_id = :id AND department_id =:department_id";
			$params = array('id'=>$item_id,'department_id'=>$data["department_id"]);
			$this->connection->DeleteQuery($sql,$params);		

			$sql = "INSERT INTO department_item(department_id,item_id, department_item_min , department_item_max, department_item_current, user_id, modified_date) 
			VALUES(:department_id, :item_id, :department_item_min, :department_item_max, :department_item_current, :user_id, :mDate)";
			$params = array('department_id'=>$data['department_id'], 'item_id'=>$item_id, 'department_item_min'=>$value['min'],  'department_item_max'=>$value['max'],'department_item_current'=>0, 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
			$department_item_id =$this->connection->InsertQuery($sql, $params);	
		}
	}

	
public function getItemsDept($department_id)
	{
		$sql = "SELECT DISTINCT(item.item_id), item.item_name, dept.department_item_min,dept.department_item_max, dept.department_name, dept.department_id FROM item JOIN
		(SELECT department.department_id, department.department_name,department_item.item_id, department_item_min, department_item_max FROM department JOIN department_item ON department.department_id = department_item.department_id) 
		as dept ON item.item_id = dept.item_id WHERE dept.department_id = :dept_id";
		$params = array('dept_id' => $department_id );
		$items = $this->connection->Query($sql, $params);
		if($items) return $items;
		else return false;

	}


public function updateDeptItems($data)
	{
		foreach ($data['item'] as $item_id => $value) {

			$sql = "UPDATE `department_item` SET `department_item_min`= :department_item_min,`department_item_max`=:department_item_max, `user_id`= :user_id,`modified_date`=:mDate WHERE item_id = :item_id AND department_id =:department_id";
			$params = array('department_id'=>$data['department_id'], 'item_id'=>$item_id, 'department_item_min'=>$value['min'],  'department_item_max'=>$value['max'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
			$department_item_id =$this->connection->UpdateQuery($sql, $params);	
		}
	}
}
?>