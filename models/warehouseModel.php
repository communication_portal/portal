<?php 
class warehouseModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function load($warehouse_id)
	{
		$sql = "SELECT warehouse_user.user_id, warehouse_user.warehouse_id,warehouse.warehouse_name, warehouse_user.user_name, warehouse_user.user_email, warehouse_user.user_phone, warehouse_user.user_access_level 
		FROM warehouse JOIN 
		(SELECT  warehouse_user.warehouse_id,users.user_id, users.user_name, users.user_email, users.user_phone, users.user_access_level 
			FROM warehouse_user JOIN users ON warehouse_user.user_id = users.user_id) as warehouse_user ON warehouse.warehouse_id = warehouse_user.warehouse_id 
WHERE warehouse.warehouse_id= :warehouse_id";
$params = array('warehouse_id'=>$warehouse_id);
$warehouse = $this->connection->Query($sql,$params);
if($warehouse) return $warehouse[0];
else {
	$wh = $this->loadByID($warehouse_id);
	if($wh) return $wh;
	else return false;
}
}

public function loadByID($warehouse_id)
{
	$sql = "SELECT * FROM warehouse WHERE warehouse_id = :wh_id LIMIT 1";
	$params = array('wh_id'=>$warehouse_id);
	$wh = $this->connection->Query($sql, $params);
	if($wh) 
	{
		$wh[0]['user_id'] = null;
		return $wh[0];
	}
	else return false;
}

public function getCollection()
{
	// $sql = "SELECT * FROM (SELECT warehouse.warehouse_id, warehouse_name, warehouse_user.user_id FROM warehouse 
	// 	LEFT JOIN warehouse_user ON warehouse_user.warehouse_id = warehouse.warehouse_id) as wh LEFT JOIN users ON users.user_id = wh.user_id";
	$sql = "SELECT * FROM warehouse ORDER BY warehouse_name asc";
	$params = array("","");

	$warehouses = $this->connection->Query($sql,$params);
	if($warehouses)
	{
		return $warehouses;
	}
	else return false;
}

public function loadLotNo($item_id, $warehouse_id, $quantity)
{
	$sql = "SELECT * FROM warehouse_stock WHERE warehouse_id = :warehouse_id AND item_id = :item_id AND status = :status";
	$params = array('warehouse_id' => $warehouse_id , 'item_id'=>$item_id , 'status'=> 'remaining');
	$stock = $this->connection->Query($sql,$params);
	$lotNo = array();
	if($stock)
	{

		foreach ($stock as $lot) {
			if(($lot['quantity'] - $quantity) >= 0)
			{
				array_push($lotNo, $lot);
				break; 
			}
			else
			{
				array_push($lotNo, $lot);
			}
			
		}
	}
	if($lotNo) return $lotNo;
	else
		return false;
}

public function getWarehouseData($item_id)
{
	$sql = "SELECT * FROM warehouse JOIN warehouse_item ON warehouse.warehouse_id = warehouse_item.warehouse_id WHERE item_id = :item_id";
	$params = array("item_id"=>$item_id);

	$departments = $this->connection->Query($sql,$params);
	if($departments)
	{
		return $departments;
	}
	else return false;
}

public function insert($data)
{
	$sql = "SELECT warehouse_name FROM warehouse WHERE warehouse_name = :warehouse_name";
	$params = array('warehouse_name' => trim($data['warehouse_name']));
	$warehouse = $this->connection->Query($sql,$params);
	if($warehouse)
	{
		return CONFLICT;
	}
	else
	{
		$sql = "INSERT INTO warehouse(warehouse_name, user_id, modified_date) VALUES (:warehouse_name,:mId,:mDate)";
		$params = array('warehouse_name' => $data['warehouse_name'], 'mId' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$warehouse_id = $this->connection->InsertQuery($sql, $params);

	// $sql = "INSERT INTO warehouse_user(warehouse_id, user_id, user_modify_id, modified_date) VALUES (:warehouse_id, :user_id, :mId, :mDate)";
	// $params = array('user_id' => $data['warehouse_manager'], 'warehouse_id' => $warehouse_id, 'mId' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
	// $this->connection->InsertQuery($sql, $params);

		return $warehouse_id;
	}
}

public function edit($data)
{
	$sql = "UPDATE warehouse SET warehouse_name=:warehouse_name, user_id=:mId, modified_date=:mDate WHERE warehouse_id = :id";
	$params = array('warehouse_name'=>$data["warehouse_name"], 'id'=>$data['warehouse_id'], 'mId' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
	$this->connection->UpdateQuery($sql, $params);
}
public function delete($warehouse_id)
{
	$sql = "DELETE FROM warehouse WHERE warehouse_id = :warehouse_id";
	$params = array('warehouse_id' => $warehouse_id );
	$this->connection->DeleteQuery($sql,$params);

}
}
?>