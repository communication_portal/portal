<?php
class DesignationModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getCollection($company_id)
	{
		
		$sql = "SELECT * FROM designations WHERE company_id = :company_id";
		$params = array("company_id"=>$company_id);
		$designations = $this->connection->Query($sql,$params);
		if($designations) return $designations;
		else return false;
	}

	public function load($id)
	{
		$sql = "SELECT * FROM designations WHERE designation_id = ".$id;
		$designation = $this->connection->Query($sql);
		if($designation) return $designation[0];
		else return false;
	}

	public function getDesignationName($id)
	{
		$sql = "SELECT * FROM designations 
		LEFT JOIN employee
		ON designations.designation_id=designations.designation_id
		WHERE employee.employee_id = ".$id." LIMIT 1";
		$designationName = $this->connection->Query($sql);
		if($designationName) 
		{
			return $designationName[0];
		}
		else 
		{
			return false;
		}
	}
}