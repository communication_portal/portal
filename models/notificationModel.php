<?php
class NotificationModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getNotification()
    {
        parent::__construct();
        $sql = "DELETE FROM notification_stock WHERE quantity = 0";
        $this->connection->DeleteQuery($sql);

        $sql = "DELETE FROM notification_purchase WHERE quantity = 0";
        $this->connection->DeleteQuery($sql);
        
        $sql = "SELECT * FROM notification WHERE status = 'inprogress' ORDER BY notification_id desc";
        $notifications = $this->connection->Query($sql);
        if ($notifications) return $notifications;
        else return false;
    }


    public function getNotificationDetail($type, $id, $dept_id=false, $item_id=false)
    {
        parent::__construct();
        if($dept_id)
        {
            $sql = "SELECT * FROM notification_".$type." WHERE notification_".$type."_id = :notification_stock_id AND department_id = :dept_id";
            $params = array('notification_stock_id'=>$id,'dept_id'=>$dept_id);
        }
        else if($item_id)
        {
            $sql = "SELECT * FROM notification_".$type." WHERE notification_".$type."_id = :notification_stock_id AND item_id = :item_id";
            $params = array('notification_stock_id'=>$id,'item_id'=>$item_id);

        }
        else 
        {
            $sql = "SELECT * FROM notification_".$type." WHERE notification_".$type."_id = :notification_stock_id";
            $params = array('notification_stock_id'=>$id);

        }
            // $this->connection->DebugQuery($sql,$params);
        $notification = $this->connection->Query($sql,$params);
        if ($notification) return $notification[0];
        else return false;
    }

  public function getNotificationDetailById($type, $item_id)
    {
      parent::__construct();

      
      $sql = "SELECT * FROM notification_".$type." WHERE item_id=:item_id";
      $params = array('item_id'=>$item_id);
        // $this->connection->DebugQuery($sql,$params);
      $notification = $this->connection->Query($sql,$params);
      if ($notification) return $notification;
      else return false;
  }

  public function stock($data, $lotNo)
  {
     $sql = "DELETE FROM notification_stock WHERE quantity = 0";
     $this->connection->DeleteQuery($sql);

     $sql = "UPDATE warehouse_stock  SET status = :status , user_id = :user_id, modified_date=:mDate  WHERE quantity = :qty ";
     $params = array('status' => 'finished', 'qty' => 0 , 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
     $this->connection->UpdateQuery($sql,$params);

     foreach ($lotNo as $lot) {
        $qty = "";
        $sql = "SELECT warehouse_id FROM warehouse_stock WHERE warehouse_stock_id = :id";
        $params = array('id' => $lot['warehouse_stock_id']);
        $warehouse_id = $this->connection->Query($sql,$params)[0]['warehouse_id'];


        $sql = "SELECT warehouse_item_current FROM warehouse_item WHERE item_id = :item_id AND warehouse_id = :warehouse_id";
        $params = array('item_id' => $data['item_id'], 'warehouse_id' => $warehouse_id);
        $warehouse_current = $this->connection->Query($sql,$params)[0]['warehouse_item_current'];

        if($data["stockQty"] < $lot['quantity'])
        {
            $qty = $lot['quantity'] - $data['stockQty'];
            $sql = "UPDATE warehouse_stock SET quantity = :qty, user_id = :user_id, modified_date=:mDate WHERE warehouse_stock_id = :id";
            $params = array('qty' => $qty , 'id' => $lot['warehouse_stock_id'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "SELECT department_item_current FROM department_item WHERE department_id = :id AND item_id = :item_id";
            $params = array('id' => $data['department_id'], 'item_id' => $data['item_id']);
            $current = $this->connection->Query($sql,$params)[0]['department_item_current'];
            $current += $data['stockQty'];
            $qty = $warehouse_current - $data['stockQty'];

            $sql = "UPDATE department_item SET department_item_current = :qty, user_id = :user_id, modified_date=:mDate WHERE department_id = :id AND item_id = :item_id";
            $params = array('qty' => $current , 'id' => $data['department_id'], 'item_id' => $data['item_id'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "UPDATE warehouse_item SET warehouse_item_current = :qty, user_id = :user_id, modified_date=:mDate WHERE item_id = :item_id AND warehouse_id = :warehouse_id";
            $params = array('item_id' => $data['item_id'], 'warehouse_id' => $warehouse_id, 'qty' =>$qty, 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "INSERT INTO `transfer`(`warehouse_id`, `department_id`, `item_id`, `lotno`, `quantity`, `date_lotno`, `user_id`,`modified_date`) VALUES (:warehouse_id, :department_id, :item_id, :lotno, :quantity, :date_lotno, :user_id, :mDate)";
            $params = array('warehouse_id' => $warehouse_id, 'department_id' => $data['department_id'], 'item_id' => $data['item_id'], 'lotno' => $lot['lot_no'], 'quantity' => $data["stockQty"] , 'date_lotno' => date('Y-m-d') , 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->InsertQuery($sql,$params);

            break;               
        }
        else 
        {
            $data['stockQty'] -= $lot['quantity'];
            $sql = "UPDATE warehouse_stock SET quantity = :qty, user_id = :user_id, modified_date=:mDate  WHERE warehouse_stock_id = :id";
            $params = array('qty' => "0" , 'id' => $lot['warehouse_stock_id'] , 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "SELECT department_item_current FROM department_item WHERE department_id = :id AND item_id = :item_id";
            $params = array('id' => $data['department_id'], 'item_id' => $data['item_id']);
            $current = $this->connection->Query($sql,$params)[0]['department_item_current'];
            $current += $lot['quantity'];
            $qty = $warehouse_current - $lot['quantity'];

            $sql = "UPDATE department_item SET department_item_current = :qty , user_id = :user_id, modified_date=:mDate WHERE department_id = :id AND item_id = :item_id";
            $params = array('qty' => $current , 'id' => $data['department_id'], 'item_id' => $data['item_id'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "UPDATE warehouse_item SET warehouse_item_current = :qty , user_id = :user_id, modified_date=:mDate WHERE item_id = :item_id AND warehouse_id = :warehouse_id";
            $params = array('item_id' => $data['item_id'], 'warehouse_id' => $warehouse_id, 'qty' =>$qty, 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->UpdateQuery($sql,$params);

            $sql = "INSERT INTO `transfer`(`warehouse_id`, `department_id`, `item_id`, `lotno`, `quantity`, `date_lotno`, `user_id`,`modified_date`) VALUES (:warehouse_id, :department_id, :item_id, :lotno, :quantity, :date_lotno, :user_id, :mDate)";
            $params = array('warehouse_id' => $warehouse_id, 'department_id' => $data['department_id'], 'item_id' => $data['item_id'], 'lotno' => $lot['lot_no'], 'quantity' => $lot['quantity'] , 'date_lotno' => date('Y-m-d') , 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
            $this->connection->InsertQuery($sql,$params);
        }

    }

    $sql = "UPDATE warehouse_stock  SET status = :status , user_id = :user_id, modified_date=:mDate  WHERE quantity = :qty ";
    $params = array('status' => 'finished', 'qty' => 0 , 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
    $this->connection->UpdateQuery($sql,$params);

    $sql = "DELETE FROM notification_stock WHERE quantity = 0";
    $this->connection->DeleteQuery($sql);

}

public function purchase($data)
{
  $sql = "INSERT INTO purchase_order(item_id,vendor_id,purchase_item_ETA,purchase_item_status,purchase_item_date,purchase_item_quantity, user_id, modified_date) 
  VALUES (:item_id,:vendor_id,:purchase_item_ETA,:purchase_item_status,:purchase_item_date,:purchase_item_quantity,:user_id,:mDate)";
  $params = array('item_id' => $data['item_id'],'vendor_id' => $data['vendor_select'],'purchase_item_ETA' => $data['datePicker'],'purchase_item_status' => 'inprogress','purchase_item_date' => date('Y-m-d'),'purchase_item_quantity' => $data['purchase_qty'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
  $this->connection->InsertQuery($sql, $params);
}

public function loadData($purchaseId)
{
    $sql = "SELECT * FROM purchase_order WHERE purchase_item_id = :id ";
    $params = array('id' => $purchaseId);
    $data = $this->connection->Query($sql,$params)[0];

    if($data) return $data;
    else return false;
}

public function delayETA($data)
{
 $sql = "UPDATE purchase_order SET purchase_item_ETA = :etaDate ,purchase_item_status = :status, user_id=:user_id, modified_date=:mDate WHERE purchase_item_id = :id";
 $params = array('id' => $data['purchase_id'], 'etaDate' => $data['eta_date'], 'status' => 'inprogress', 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
 $this->connection->UpdateQuery($sql, $params);

 $sql = "UPDATE notification_eta SET etaDate = :etaDate WHERE notification_ETA_id = :id";
 $params = array('id' => $data['eta_id'], 'etaDate' => $data['eta_date']);
 $this->connection->UpdateQuery($sql, $params);

}

public function rejectETA($purchaseId)
{

    $sql = "UPDATE purchase_order SET purchase_item_status = 'rejected', user_id = :user_id, modified_date=:mDate WHERE purchase_item_id = :id";
    $params = array('id' => $purchaseId,'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
    // $this->connection->DebugQuery($sql, $params);
    $this->connection->UpdateQuery($sql, $params);

    $sql = "SELECT notification_ETA_id FROM notification_eta WHERE purchase_id = :id";
    $params = array('id' => $purchaseId);
    $eta_id = $this->connection->Query($sql, $params)[0]['notification_ETA_id'];

    $sql = "DELETE FROM notification_eta WHERE purchase_id = :id";
    $params = array('id' => $purchaseId);
    $this->connection->DeleteQuery($sql, $params);

    $sql = "UPDATE notification SET status = 'completed' WHERE notification_id = :id";
    $params = array('id' => $eta_id);
    $this->connection->UpdateQuery($sql, $params);

    $sql = "SELECT * FROM purchase_order WHERE purchase_item_id = :id ";
    $params = array('id' => $purchaseId);
    $data = $this->connection->Query($sql, $params)[0];

    $sql = "SELECT notification_id FROM notification ORDER BY notification_id DESC LIMIT 1";
    $current_id = $this->connection->Query($sql);

    if(!$current_id)
    {
        $next_id = 1;
    }
    else
    {
        $next_id = $current_id[0]['notification_id'] + 1;
    }
    
$sql = "DELETE FROM notification_purchase WHERE item_id = :item_id";
    $params = array('item_id'=>$data['item_id']);
    $this->connection->DeleteQuery($sql,$params);

    $sql = "INSERT INTO notification_purchase(notification_purchase_id, item_id, vendor_id, quantity) VALUES 
    (:notification_purchase_id, :item_id, :vendor_id, :quantity)";
    $params = array('notification_purchase_id' => $next_id, 'item_id' => $data['item_id'], 'vendor_id' => $data['vendor_id'], 'quantity' => $data['purchase_item_quantity']);
    $id = $this->connection->InsertQuery($sql,$params);

    $sql = "INSERT INTO notification(notification_id, notification_order, type, status) VALUES(:id, 0, :type, :status)";
    $params = array('id'=> $next_id, 'type'=> 'purchase', 'status' => 'inprogress' );
    $this->connection->InsertQuery($sql,$params);
}

public function acceptETA($data)
{
    $sql = "UPDATE purchase_order SET purchase_item_status = 'completed', user_id = :user_id, modified_date=:mDate WHERE purchase_item_id = :id";
    $params = array('id' => $data['purchase_id'],'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
    $this->connection->UpdateQuery($sql, $params);

    $sql = "SELECT notification_ETA_id FROM notification_eta WHERE purchase_id = :id";
    $params = array('id' => $data['purchase_id']);
    $eta_id = $this->connection->Query($sql, $params)[0]['notification_ETA_id'];

    $sql = "DELETE FROM notification_eta WHERE purchase_id = :id";
    $params = array('id' => $data['purchase_id']);
    $this->connection->DeleteQuery($sql, $params);

    $sql = "UPDATE notification SET status = 'completed' WHERE notification_id = :id";
    $params = array('id' => $eta_id);
    $this->connection->UpdateQuery($sql, $params);

    $sql = "SELECT * FROM purchase_order WHERE purchase_item_id = :id ";
    $params = array('id' => $data['purchase_id']);
    $purchase_data = $this->connection->Query($sql, $params)[0];

    $sql = "SELECT warehouse_item_current FROM warehouse_item WHERE item_id = :id";
    $params = array('id' => $purchase_data['item_id']);
    $current = $this->connection->Query($sql,$params);

    if($current)
    {
        $current = $current[0]['warehouse_item_current'];
    }
    else
    {
        $current = 0;
    }
    $sql = "UPDATE warehouse_item SET warehouse_item_current = :qty, user_id=:user_id, modified_date=:mDate WHERE item_id = :item_id";
    $params = array('item_id' => $purchase_data['item_id'], 'qty' => ($current + $purchase_data['purchase_item_quantity']),'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
    $this->connection->UpdateQuery($sql, $params);
    // var_dump($data);
    // var_dump($purchase_data);

    $sql = "SELECT warehouse_id FROM warehouse_item WHERE item_id = :item_id";
    $params = array('item_id' => $purchase_data['item_id']);
    $warehouse_id =$this->connection->Query($sql, $params)[0]['warehouse_id'];

    $sql = "INSERT INTO `warehouse_stock`(`warehouse_id`, `item_id`, `quantity`, `lot_no`, `date_lotno`, `status`, `user_id`, `modified_date`) VALUES (:warehouse_id, :item_id, :quantity, :lotNo, :date_lotno, :status, :user_id, :mDate)";
    $params = array('warehouse_id' => $warehouse_id, 'item_id' => $purchase_data['item_id'], 'quantity' => $purchase_data['purchase_item_quantity'], 'lotNo' => $data['lot_no'], 'date_lotno' => date('Y-m-d'), 'status' => 'remaining','user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
    $this->connection->InsertQuery($sql,$params);

    $sql = "DELETE FROM notification_purchase WHERE quantity = 0";
    $this->connection->DeleteQuery($sql);

}



}