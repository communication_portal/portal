<?php
class EmployeeModel extends Model
{
	public function __construct()
	{
		loadHelper('url');
		parent::__construct();
	}

	public function getCollectionByCompanyId($id)
	{
		$sql = "SELECT * FROM employee WHERE company_id=:id";
		$params = array('id'=>$id);
		$result = $this->connection->Query($sql, $params);
		//var_dump($id);die;
		if($result)	
			return $result;
		else
			return false;
	}

	private function insertEmployee($data)
	{
		$sql = "INSERT INTO employee(name, dob, gender, photo,phone_no,perm_add, temp_add, city, nationality, zipCode ,company_id, designation_id) VALUES 
		(:name, :dob, :gender, :photo, :phone_no,:perm_add, :temp_add, :city, :nationality, :zipCode,:company_id, :designation_id)";
		$params = array("name"=>$data['name'] , "dob"=> $data['dob'], "gender" =>$data['gender'],"photo"=>$data['photo'],"phone_no"=>$data['phone'], "perm_add"=>$data['perm_add'],"temp_add"=>$data['temp_add'],"zipCode"=>$data["zipCode"],"nationality"=>$data['nationality'],"city"=>$data["city"],"company_id"=>$data['company'],"designation_id"=>$data['designation']);
		$user_id = $this->connection->DebugQuery($sql, $params);
		return $user_id;

	}

	private function insertLogin($data,$user_id)
	{
		$sql = "INSERT INTO login(employee_id, username , email,password) VALUES (:user_id, :username, :email, :password)";
		$params = array('user_id' => $user_id, 'username'=> $username,'email' => $data['email'],'password' => md5($data['password']));
		$this->connection->InsertQuery($sql, $params);
	}

	private function checkUserExist($data)
	{
		$sql = "SELECT * FROM login WHERE email = :email";
		$params = array('email' => $data['email']);
		$email = $this->connection->Query($sql,$params);
		return $email;
	}

	public function insert($data)
	{
		$result = $this->checkUserExist($data);
		if($email)
		{
			Session::AddErrorMessage('Employee with this Email already exists');
			return false;
		}
		else
		{
			//Insert into employee
			$user_id = $this->insertEmployee($data);
			$this->insertLogin($data,$user_id);
			//insert work experience
			//insert qualification
			return $user_id;
		}
	}

	public function getInfoById($id)
	{
		$sql = "SELECT * FROM employee WHERE employee_id = :employee_id";
		$params = array("employee_id"=>$id);
		return $this->connection->Query($sql,$params)[0];
	}


	public function editUserPassword($data)
	{

		$sql = "SELECT user_id FROM user_password WHERE token_id =:id";
		$params = array('id' => $data['token_id'] );
		$user_id = $this->connection->Query($sql, $params)[0]['user_id'];

		$sql = "UPDATE users SET password =:password WHERE user_id = :id";
		$params = array('password'=>md5($data["password"]),'id'=>$user_id);
		$this->connection->UpdateQuery($sql, $params);

		$sql = "DELETE FROM user_password WHERE user_id = :user_id";
		$params = array('user_id' => $user_id );
		$this->connection->DeleteQuery($sql,$params);
	}

	public function checkSetPassword($token_id)
	{
		$sql = "SELECT * FROM user_password WHERE token_id=:id";
		$params = array('id' => $token_id);
		$result = $this->connection->Query($sql,$params);
		if($result) return true;
		else return false;
	}

	public function edit($data)
	{
		//var_dump($data['user_name']);
		if($data['user_name'] == ADMIN)
		{
			return ERROR;
		}
		$sql = "SELECT user_email FROM users WHERE user_email = :user_email AND user_id != :id";
		$params = array('id'=>$data['user_id'],'user_email'=>$data["user_email"]);
		$users = $this->connection->Query($sql,$params);
		if($users)
		{
			return CONFLICT;
		}
		else
		{
			$sql = "UPDATE users SET user_name=:user_name, photo =:photo,user_email=:user_email, user_phone=:user_phone, user_access_level=:user_access_level WHERE user_id = :id";
			$params = array('user_name'=>$data["user_name"],'photo'=>$data['photo'], 'user_email'=>$data["user_email"], 'id'=>$data['user_id'], 'user_phone'=>$data['user_phone'], 'user_access_level'=>$data['user_access_level']);
			$user = $this->connection->UpdateQuery($sql, $params);
			return NOCONFLICT;
		}

		
	}
	
	public function delete($user_id)
	{
		$sql = "DELETE FROM users WHERE user_id = :user_id AND user_name != :user_name";
		$params = array('user_id' => $user_id, 'user_name' => ADMIN);
		$this->connection->DeleteQuery($sql,$params);

		$sql = "DELETE FROM department_user WHERE user_id = :user_id";
		$params = array('user_id' => $user_id );
		$this->connection->DeleteQuery($sql,$params);

		$sql = "DELETE FROM user_password WHERE user_id = :user_id";
		$params = array('user_id' => $user_id );
		$this->connection->DeleteQuery($sql,$params);
	}

	//if user is logged in then donot allow to delete that user
	//if user that is logged in is deleted then logout
	//check email after password reset


}
?>