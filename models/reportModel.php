<?php
/**
* 
*/
class ReportModel extends Model
{
	
	public function __construct()
	{
		parent::__construct();

	}

	public function loadDept($data)
	{
		if($data['periods']!="")
		{
			$periods = $data['periods'];
			$dates = $this->getDateRange($periods);
			$sql = "SELECT stock.item_id, stock.item_name, stock.stock, usage1.used FROM (SELECT item.item_name, item.item_id, SUM(quantity) AS stock FROM `transfer` JOIN item ON item.item_id = transfer.item_id WHERE date_lotno between :from_date and :to_date GROUP BY item.item_id) as stock LEFT JOIN (SELECT item_id, SUM(daily_usage_quantity) AS used FROM `daily_usage` WHERE usage_date between :from_usagedate and :to_usagedate GROUP BY item_id) AS usage1 ON usage1.item_id = stock.item_id";
			$params = array('from_date' => $dates['start_date'], 'to_date' => $dates['end_date'],'from_usagedate' => $dates['start_date'], 'to_usagedate' => $dates['end_date']);
			$report['data'] = $this->connection->Query($sql, $params);
			$report['start_date'] = $dates['start_date'];
			$report['end_date'] = $dates['end_date'];

			
		}
		else
		{
			$sql = "SELECT stock.item_id, stock.item_name, stock.stock, usage1.used FROM (SELECT item.item_name, item.item_id, SUM(quantity) AS stock FROM `transfer` JOIN item ON item.item_id = transfer.item_id WHERE date_lotno between :from_date and :to_date GROUP BY item.item_id) as stock LEFT JOIN (SELECT item_id, SUM(daily_usage_quantity) AS used FROM `daily_usage` WHERE usage_date between :from_usagedate and :to_usagedate GROUP BY item_id) AS usage1 ON usage1.item_id = stock.item_id";
			$params = array('from_date' => $data['date_from'], 'to_date' => $data['date_to'],'from_usagedate' => $data['date_from'], 'to_usagedate' => $data['date_to']);
			$report['data'] = $this->connection->Query($sql, $params);
			$report['start_date'] =  $data['date_from'];
			$report['end_date'] =  $data['date_to'];
		}

		return $report;
	}

	public function loadItem($data)
	{
		if($data['periods']!="")
		{
			$periods = $data['periods'];
			$dates = $this->getDateRange($periods);
			$sql = "SELECT * FROM (SELECT stock.item_id, stock.item_name, stock.stock, usage1.used FROM 
					(SELECT item.item_name, item.item_id, SUM(quantity) AS stock FROM `transfer` JOIN item ON item.item_id = transfer.item_id WHERE date_lotno between :from_date and :to_date GROUP BY item.item_id) as stock 
					LEFT JOIN (SELECT item_id, SUM(daily_usage_quantity) AS used FROM `daily_usage` WHERE usage_date between :from_usagedate and :to_usagedate GROUP BY item_id)
 					AS usage1 ON usage1.item_id = stock.item_id) as stocks 
					LEFT JOIN (SELECT item_id, SUM(purchase_item_quantity) AS purchased FROM `purchase_order` WHERE purchase_item_ETA between :from_purchasedate and :to_purchasedate AND purchase_item_status = :status GROUP BY item_id) 
					AS purchase ON purchase.item_id = stocks.item_id";
			$params = array('from_date' => $dates['start_date'], 'to_date' => $dates['end_date'],'from_purchasedate' => $dates['start_date'], 'to_purchasedate' => $dates['end_date'],'from_usagedate' => $dates['start_date'], 'to_usagedate' => $dates['end_date'], 'status' =>'completed');
			$report['data'] = $this->connection->Query($sql, $params);
			$report['start_date'] = $dates['start_date'];
			$report['end_date'] = $dates['end_date'];

			
		}
		else
		{
			$sql = "SELECT * FROM (SELECT stock.item_id, stock.item_name, stock.stock, usage1.used FROM 
					(SELECT item.item_name, item.item_id, SUM(quantity) AS stock FROM `transfer` JOIN item ON item.item_id = transfer.item_id WHERE date_lotno between :from_date and :to_date GROUP BY item.item_id) as stock 
					LEFT JOIN (SELECT item_id, SUM(daily_usage_quantity) AS used FROM `daily_usage` WHERE usage_date between :from_usagedate and :to_usagedate GROUP BY item_id)
 					AS usage1 ON usage1.item_id = stock.item_id) as stocks 
					LEFT JOIN (SELECT item_id, SUM(purchase_item_quantity) AS purchased FROM `purchase_order` WHERE purchase_item_ETA between :from_purchasedate and :to_purchasedate AND purchase_item_status = :status GROUP BY item_id) 
					AS purchase ON purchase.item_id = stocks.item_id";
			$params = array('from_date' => $data['date_from'], 'to_date' => $data['date_to'],'from_purchasedate' => $data['date_from'], 'to_purchasedate' => $data['date_to'],'from_usagedate' => $data['date_from'], 'to_usagedate' => $data['date_to'], 'status' =>'completed');
			$report['data'] = $this->connection->Query($sql, $params);
			$report['start_date'] =  $data['date_from'];
			$report['end_date'] =  $data['date_to'];
		}

		return $report;

	}

	public function getDateRange($period)
	{
		$period = explode(',',$period);
		$data['start_date'] = date('Y-m-d', strToTime($period[0]));
		$data['end_date'] = date('Y-m-d', strToTime($period[1]));

		return $data;
		
	}

}

?>