<?php
class DepartmentsModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function load($department_id)
	{
		$sql = "SELECT dept_user.user_id, dept_user.department_id,department.department_name, dept_user.user_name, dept_user.user_email, dept_user.user_phone, dept_user.user_access_level 
		FROM department JOIN 
		(SELECT  department_user.department_id,users.user_id, users.user_name, users.user_email, users.user_phone, users.user_access_level 
			FROM department_user JOIN users ON department_user.user_id = users.user_id) as dept_user ON department.department_id = dept_user.department_id 
WHERE department.department_id= :department_id";
$params = array('department_id'=>$department_id);
$department = $this->connection->Query($sql,$params);

if($department) return $department[0];
else
{
	$dept = $this->loadByID($department_id);
	if($dept) return $dept;
	else return false;
}
}

public function loadByID($department_id)
{
	$sql = "SELECT * FROM department WHERE department_id = :dept_id LIMIT 1";
	$params = array('dept_id'=>$department_id);
	$dept = $this->connection->Query($sql, $params);
	if($dept) 
	{
		$dept[0]['user_id'] = null;
		return $dept[0];
	}
	else return false;
}

public function loadByUser($user_id)
{
	$departments = $this->loadDepartmentByUser($user_id);
	$dept_detail = array();
	foreach ($departments as $department) {
		array_push($dept_detail, $this->load($department['department_id']));
	}

	return $dept_detail;
}

	public function getCollection()//TODO multiple department showing
	{
		$sql = "SELECT * FROM (SELECT department.department_id, department_name, department_user.user_id FROM department LEFT JOIN 
			department_user ON department_user.department_id = department.department_id) as deps LEFT JOIN users ON users.user_id = deps.user_id ORDER BY deps.department_name asc";
$params = array("","");

$departments = $this->connection->Query($sql,$params);
if($departments)
{
	return $departments;
}
else return false;
}

public function getDistinctCollection()
{
	$sql = "SELECT GROUP_CONCAT(department_id) as department_id, department_name FROM department GROUP BY(department_name)";
	$params = array("","");

	$departments = $this->connection->Query($sql,$params);
	if($departments)
	{
		return $departments;
	}
	else return false;
}

public function checkUsageDepts()
{
	$sql = "SELECT department_id FROM department_item";
	$dept_ids = $this->connection->Query($sql);
	foreach ($dept_ids as $dept_id) {
		$sql = "SELECT daily_usage_id FROM daily_usage WHERE department_id =:dept_id AND usage_date =:usage_date";
		$params = array('dept_id' => $dept_id['department_id'], 'usage_date' => date('Y-m-d') );
		$daily_usage = $this->connection->Query($sql,$params);
		if(!$daily_usage)	
		{
			return false;
		}
	}
	return true;
}

public function checkUsage($dept_id)
{
	$sql="SELECT * FROM daily_usage WHERE usage_date = :usage_date AND department_id = :department_id";
	$params = array('usage_date' => date("Y-m-d"), 'department_id' =>$dept_id);
	$usage = $this->connection->Query($sql, $params);
	if ($usage) return true;
	else return false;

}

public function getUsageValue()
{
	$sql="SELECT * FROM daily_usage WHERE usage_date = :usage_date";
	$params = array('usage_date' => date("Y-m-d"));
	$usage = $this->connection->Query($sql, $params);

	$daily_usage  = array();
	if($usage) 
	{
		foreach ($usage as $u) {
			$daily_usage[$u['department_id']][$u['item_id']] = $u['daily_usage_quantity'];
		}
		return $daily_usage;

	}
	else return false;
}

public function insertUsage($usage,$department_id,$date, $prev_usage)
{
	$sql = "INSERT INTO daily_usage(department_id, item_id, daily_usage_quantity, usage_date, manager_id, user_id, modified_date)
	VALUES (:department_id, :item_id, :daily_usage_quantity, :usage_date, :manager_id,:user_id, :mDate)";
	$params = array('department_id' => $department_id, 'item_id' => $usage['item_id'], 'daily_usage_quantity' => $usage['daily_usage'], 'usage_date' => date('Y-m-d'), 'manager_id' => Session::getSessionVariable('user_id'),'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
	$daily_usage_id = $this->connection->InsertQuery($sql, $params);

	$sql = "SELECT department_item_current FROM department_item WHERE item_id = :id AND department_id = :department_id";
	$params = array('id' => $usage['item_id'], 'department_id'=>$department_id);
	$current_item = $this->connection->Query($sql, $params);

			//var_dump((int)$current_item[0]['department_item_current']);
	$current = ((float)$current_item[0]['department_item_current']) + ((float)$prev_usage - ((float)$usage['daily_usage']));
			//echo $current;
	$sql = "UPDATE department_item SET department_item_current=:department_item_current, user_id=:user_id, modified_date=:mDate WHERE item_id = :id AND department_id = :dept_id";
	$params = array('department_item_current'=>$current, 'id'=>$usage['item_id'], 'dept_id'=>$department_id, 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
	$this->connection->UpdateQuery($sql, $params);

}

public function deleteUsage($department_id,$date)
{
	$sql = "SELECT * FROM daily_usage";
	$daily_usage = $this->connection->Query($sql);
	$usage = array();
	foreach ($daily_usage as $dusage) {
		$usage[$dusage['department_id']][$dusage['item_id']] = $dusage['daily_usage_quantity'];
	}
	$sql = "DELETE FROM daily_usage WHERE department_id = :department_id";
	$params = array('department_id' => $department_id );
	$this->connection->DeleteQuery($sql,$params);

	return $usage;
}

public function getDepartmentData($item_id)
{
	$sql = "SELECT * FROM department JOIN department_item ON department.department_id = department_item.department_id WHERE item_id = :item_id";
	$params = array("item_id"=>$item_id);

	$departments = $this->connection->Query($sql,$params);
	if($departments)
	{
		return $departments;
	}
	else return false;
}

public function loadDepartmentByUser($user_id)
{
	$sql = "SELECT department.department_id, department.department_name FROM department JOIN department_user ON department_user.department_id = department.department_id WHERE department_user.user_id = :id";
	$params = array('id' => $user_id);
	$department = $this->connection->Query($sql,$params);
	if($department) return $department;
	else return false;
}

public function insert($data)
{
	$sku = $data['department_name']."_".$data['user'];
			//var_dump($sku);die;
	$sql = "SELECT SKU FROM department WHERE SKU = :sku";
	$params = array('sku' => $sku);
	$checkSKU = $this->connection->Query($sql, $params)[0]['SKU'];

	if($checkSKU == NULL)
	{

		$sql = "INSERT INTO department(department_name, user_id, modified_date) VALUES (:department_name, :user_id,  :mDate)";
		$params = array('department_name' => $data['department_name'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$department_id = $this->connection->InsertQuery($sql, $params);

		$sql = "INSERT INTO department_user(department_id,user_id, user_modify_id, modified_date) VALUES (:department_id,:user_id, :mId, :mDate)";
		$params = array('user_id' => $data['user'], 'department_id' => $department_id, 'mId' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$this->connection->InsertQuery($sql, $params);

		$sku = $data['department_name']."_".$data['user'];


		$sql = "UPDATE department SET SKU = :SKU WHERE department_id =:dept_id";
		$params = array('SKU' => $data['department_name']."_".$data['user'], 'dept_id' => $department_id);
		$this->connection->UpdateQuery($sql,$params);

		return $department_id;
	}

	return false;
}

public function edit($data)
{
	$user_id = Session::getSessionVariable('user_id');
	$sql = "UPDATE department SET department_name=:department_name, user_id=:user_id, modified_date=:mDate WHERE department_id = :id";
	$params = array('department_name'=>$data["department_name"], 'id'=>$data['department_id'], 'user_id' => $user_id, 'mDate' => date('Y-m-d'));
	$this->connection->UpdateQuery($sql, $params);

	$sql ="SELECT user_id, department_id FROM department_user WHERE department_id = :id";
	$params = array('id' =>  $data['department_id']);
	$dept_users = $this->connection->Query($sql,$params);
	if($dept_users)
	{
		$sql = "UPDATE department_user SET user_id=:user_id, user_modify_id=:mId, modified_date=:mDate WHERE department_id = :id";
		$params = array('user_id'=>$data["user"], 'id'=>$data['department_id'], 'mId' => $user_id, 'mDate' => date('Y-m-d'));
		$this->connection->UpdateQuery($sql, $params);
	}
	else
	{
		$sql = "INSERT INTO department_user(department_id, user_id, user_modify_id, modified_date) VALUES(:id, :user_id,:user_modify_id,:mDate)";
		$params = array('user_id'=>$data["user"], 'id'=>$data['department_id'], 'user_modify_id' => $user_id, 'mDate' => date('Y-m-d'));
		$this->connection->InsertQuery($sql, $params);
	}



}

public function delete($department_id)
{
	$sql = "SELECT department_item_current, item_id FROM department_item WHERE department_id = :department_id";
	$params = array('department_id' => $department_id );
	$dept_item = $this->connection->Query($sql, $params);

	foreach ($dept_item as $item) {
		$sql = "SELECT warehouse_item_current FROM warehouse_item WHERE item_id = :item_id";
		$params = array('item_id' =>$item['item_id'] );
		$wh = $this->connection->Query($sql,$params);

		$sql = "UPDATE warehouse_item SET warehouse_item_current = :current WHERE item_id = :item_id";
		$params = array('current' => $wh[0]['warehouse_item_current'] + $item['department_item_current'], 'item_id'=>$item['item_id']);
		$this->connection->UpdateQuery($sql, $params);
	}

	$sql = "DELETE FROM department WHERE department_id = :department_id";
	$params = array('department_id' => $department_id );
	$this->connection->DeleteQuery($sql,$params);

	$sql = "DELETE FROM department_user WHERE department_id = :department_id";
	$params = array('department_id' => $department_id );
	$this->connection->DeleteQuery($sql,$params);
}

}
?>