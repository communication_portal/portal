<?php
/**
* 
*/
class CheckModel extends Model
{
	
	public function __construct()
	{
		parent::__construct();

	}

	public function compare()
	{
		// echo "<pre>";
		$inputFileName = 'assets/uploads/documents/inventory.xlsx';
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		$sheetCount = $objPHPExcel->getSheetCount();
		$sheetCount-=2;

		for ($sheetNo = 3; $sheetNo <= 16; $sheetNo++) {
			$sheet = $objPHPExcel->getSheet($sheetNo); 
			$worksheetTitle = $sheet->getTitle();
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestDataColumn();
			$colIndex = ($worksheetTitle == "Packaging")? PHPExcel_Cell::columnIndexFromString($highestColumn) - 1:PHPExcel_Cell::columnIndexFromString($highestColumn);

			if (($handle = fopen("inventory.csv", "r")) !== FALSE)
			{
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
				{
					die('here');
					for ($column = 2; $column != $colIndex; $column+=6){ 
						$title = $sheet->getCellByColumnAndRow($column , '1')->getValue();
						$TIH = $sheet->getCellByColumnAndRow($column ,'37')->getCalculatedValue();
						$whColumn = $column + 1;
						$warehouse = $sheet->getCellByColumnAndRow($whColumn , '37')->getCalculatedValue();
						if($worksheetTitle != "Shared Supplies")
						{
							$dptColumn = $column + 3;
							$department = $sheet->getCellByColumnAndRow($dptColumn,'37')->getCalculatedValue();
						}

						if(strtolower($title) == $data[3] && strtolower($worksheetTitle) == $data[1] && $data[2] != 0)
						{
							if($worksheetTitle != "Shared Supplies")
							{
								$sql = 'SELECT department_item_current FROM department_item WHERE item_id = :item_id AND department_id = :department_id';
								$params = array('item_id'=>$data[2], 'department_id'=>$data[0]);
								$connection = $this->connection->Query($sql,$params);
								$result['dept_current'] = $connection[0]['department_item_current'];

								$sql = 'SELECT warehouse_item_current FROM warehouse_item WHERE item_id = :item_id';
								$params = array('item_id'=>$data[2]);
								$connection = $this->connection->Query($sql,$params);
								$result['wh_current'] = $connection[0]['warehouse_item_current'];
								if($result['wh_current'] != null && $result['dept_current'] != null	&& $warehouse != null && $department != null)
								{
									if($result['dept_current'] != $department || $result['wh_current'] != $warehouse)
									{
										var_dump($worksheetTitle);
										var_dump($data[3]);
										$values[$data[1]][$data[3]]['wh_current_inventory'] = $result['wh_current'];
										echo "warehouse inventory ";
										var_dump($result['wh_current']);
										$values[$data[1]][$data[3]]['wh_current_excel'] = $warehouse;
										echo "warehouse excel ";
										var_dump($warehouse);
										$values[$data[1]][$data[3]]['dept_current_inventory'] = $result['dept_current'];
										echo "department inventory ";
										var_dump($result['dept_current']);
										$values[$data[1]][$data[3]]['dept_current_excel'] = $department;
										echo "department excel ";
										var_dump($department);
										echo "\n";
									}
								}
							}
						}
					}
				}
				fclose($handle);
			}
		}
		die;

		return $values;
	}

}

?>