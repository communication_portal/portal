<?php
class CompanyModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function load($id)
	{
		$sql = "SELECT * FROM vendor WHERE vendor_id = :vendor_id";
		$params = array("vendor_id"=>$id);

		$vendor = $this->connection->Query($sql, $params);
		if($vendor)return $vendor;
		else return false;
	}

	public function getCollection()
	{
		
		$sql = "SELECT * FROM companies";
		$params = array("","");
		
		$companies = $this->connection->Query($sql,$params);
		if($companies)
		{
			return $companies;
		}
		else return false;

	}

	public function getVendors($item_id = false)
	{	
		$sql = "SELECT * FROM vendor JOIN vendor_item ON vendor.vendor_id = vendor_item.vendor_id WHERE vendor_item.item_id = :item_id";
		$params = array("item_id"=>$item_id);

		$items = $this->connection->Query($sql, $params);
		if($items) return $items;
		else return false;
	}

	public function insertVendors($data)
	{ 
		$sql = "SELECT vendor_name FROM vendor WHERE vendor_name = :vendorname";
		$params = array('vendorname' => trim($data['vendor_name']));
		$vendors = $this->connection->Query($sql,$params);
		if($vendors)
		{
			return CONFLICT;
		}
		else
		{
			$sql = "INSERT INTO vendor(vendor_name,contact_person,vendor_address, vendor_email, vendor_contact, vendor_website, lead_time, vendor_note, user_id, modified_date ) VALUES(:vendor_name, :contact_person, :vendor_address, :vendor_email, :vendor_contact, :vendor_website, :lead_time, :vendor_note,:user_id, :mDate)";
			$params = array('vendor_name'=>trim($data['vendor_name']),'contact_person'=>trim($data['vendor_contact_person']),'vendor_address'=>$data['vendor_address'], 'vendor_email'=>$data['vendor_email'], 'vendor_contact'=>$data['vendor_contact'], 'lead_time'=>$data['lead_time'],'vendor_note'=>$data['vendor_note'],'vendor_website'=>$data["vendor_website"], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
			$vendor_id = $this->connection->InsertQuery($sql, $params);
			return $vendor_id;
		}
	}

	public function editVendor($data)
	{
		$sql = "SELECT vendor_name FROM vendor WHERE vendor_name = :vendorname AND vendor_id != :vendor_id";
		$params = array('vendor_id'=>$data['vendor_id'],'vendorname' => trim($data['vendor_name']));
		$vendors = $this->connection->Query($sql,$params);
		if($vendors)
		{
			return false;
		}
		else
		{
			$sql = "UPDATE vendor SET vendor_name=:vendor_name, contact_person=:contact_person, vendor_address=:vendor_address, vendor_email=:vendor_email, vendor_contact=:vendor_contact, vendor_website=:vendor_website,lead_time=:lead_time, vendor_note=:vendor_note, user_id =:mId, modified_date=:mDate WHERE vendor_id=:vendor_id";
			$params = array('vendor_id'=>$data['vendor_id'],'contact_person'=>$data['vendor_contact_person'],'vendor_name'=>$data['vendor_name'],'lead_time'=>$data['lead_time'],'vendor_note'=>$data['vendor_note'],'vendor_address'=>$data['vendor_address'], 'vendor_email'=>$data['vendor_email'], 'vendor_contact'=>$data['vendor_contact'], 'vendor_website'=>$data["vendor_website"], 'mId' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
			$this->connection->UpdateQuery($sql, $params);
			return true;
		}
	}

	public function deleteVendor($vendor_id)
	{
		$sql = "DELETE FROM vendor WHERE vendor_id = :id";
		$params = array('id'=>$vendor_id);
		$this->connection->DeleteQuery($sql, $params);
	}

	public function viewVendor($vendor_id)
	{
		$sql = "SELECT * FROM vendor WHERE vendor_id = :id";
		$params = array('id'=>$vendor_id);
		$vendors = $this->connection->Query($sql, $params);
		if ($vendors) return $vendors;
		else return false;
	}

	public function getVendorInfo($vendor_id)
	{
		$sql = "SELECT lead_time, vendor_note FROM vendor WHERE vendor_id = :id";
		$params = array('id'=>$vendor_id);
		$vendors = $this->connection->Query($sql, $params);
		if ($vendors) return $vendors;
		else return false;
	}
}
?>