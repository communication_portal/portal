<?php
class PurchaseModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function purchase($data)
	{
		$sql = "INSERT INTO purchase_order(item_id,vendor_id,purchase_item_status,purchase_item_date, lot_no, purchase_item_quantity, user_id, modified_date) 
		VALUES (:item_id,:vendor_id,:purchase_item_status,:purchase_item_date,:lotNo,:purchase_item_quantity,:user_id,:mDate)";
		$params = array('item_id' => $data['item_id'],'vendor_id' => $data['vendor_select'],'purchase_item_status' => 'completed','lotNo' => date("Ymd").'_'.$data['item_id'].'_'.$data['purchase_qty'],'purchase_item_date' => date('Y-m-d'),'purchase_item_quantity' => $data['purchase_qty'], 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$this->connection->InsertQuery($sql, $params);


		$sql = "SELECT warehouse_item_current FROM warehouse_item WHERE item_id = :id";
		$params = array('id' => $data['item_id']);
		$current = $this->connection->Query($sql,$params);

		if($current)
		{
			$current = $current[0]['warehouse_item_current'];
		}
		else
		{
			$current = 0;
		}
		$sql = "UPDATE warehouse_item SET warehouse_item_current = :qty, user_id=:user_id, modified_date=:mDate WHERE item_id = :item_id";
		$params = array('item_id' => $data['item_id'], 'qty' => ($current + $data['purchase_qty']),'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$this->connection->UpdateQuery($sql, $params);
    // var_dump($data);
    // var_dump($purchase_data);

		$sql = "SELECT warehouse_id FROM warehouse_item WHERE item_id = :item_id";
		$params = array('item_id' => $data['item_id']);
		$warehouse_id =$this->connection->Query($sql, $params)[0]['warehouse_id'];

		$sql = "INSERT INTO `warehouse_stock`(`warehouse_id`, `item_id`, `quantity`, `lot_no`, `date_lotno`, `status`, `user_id`, `modified_date`) VALUES (:warehouse_id, :item_id, :quantity, :lotNo, :date_lotno, :status, :user_id, :mDate)";
		$params = array('warehouse_id' => $warehouse_id, 'item_id' => $data['item_id'], 'quantity' => $data['purchase_qty'], 'lotNo' => date("Ymd").'_'.$data['item_id'].'_'.$data['purchase_qty'], 'date_lotno' => date('Y-m-d'), 'status' => 'remaining','user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d'));
		$this->connection->InsertQuery($sql,$params);
	}

	
}
?>