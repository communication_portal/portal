<?php
class LotnoModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function load($item_id)
	{
		$sql = "SELECT * FROM warehouse_stock JOIN item ON item.item_id = warehouse_stock.item_id WHERE item.item_id =:id AND status =:status";
		$params = array('id' => $item_id , 'status' => 'remaining' );
		$lotno = $this->connection->Query($sql,$params);
		if($lotno) return $lotno;
		else return false;
	}

	public function save($lotno, $quantity)
	{
		$sql = "UPDATE warehouse_stock SET quantity = :qty, user_id=:user_id, modified_date=:mDate WHERE lot_no =:lotno";
		$params = array('qty' => $quantity, 'lotno' => $lotno, 'user_id' => Session::getSessionVariable('user_id'), 'mDate' => date('Y-m-d') );
		$this->connection->UpdateQuery($sql,$params);
	}

}
?>