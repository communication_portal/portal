<?php
class TeamModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function loadTeam($id)
	{
		$sql = "SELECT * FROM team WHERE team_id=:team_id";
		$params = array('team_id' => $id);
		$team = $this->connection->Query($sql, $params);
		if($team)
			return $team;
		else
			return false;
	}

	public function getCollection()
	{
		$sql = "SELECT * FROM team";
		$params = array('' => '');
		$group = $this->connection->Query($sql,$params);
		if($group)
			return $group;
		else
			return false;

	}

	public function insert($data)
	{
		$sql = "SELECT team_name FROM team WHERE team_name = :teamname";
		$params = array('teamname' => trim($data['team_name']));
		$group = $this->connection->Query($sql,$params);
		if($group)
		{
			return CONFLICT;
		}
		else
		{
			$sql = "INSERT INTO team(team_name,team_created_by, company_id) VALUES(:team_name, :created_by, :company_id)";
			$params = array('team_name'=>$data["team_name"], 'created_by'=>$data['created_by'], 'company_id' =>$data['company_id']);
			$team_id = $this->connection->InsertQuery($sql, $params);

			foreach($data['members'] as $member)
			{	
				$sql = "INSERT INTO team_members(team_id,employee_id) VALUES(:team_id, :employee_id)";
				$params = array('team_id'=>$team_id, 'employee_id'=>$member);
				$team_member_id =$this->connection->InsertQuery($sql, $params);
			}

			$sql = "INSERT INTO team_share(team_id, access_level) VALUES(:team_id, :access_level)";
			$params = array('team_id'=>$team_id, 'access_level'=>$data['access_level']);
			$team_share_id = $this->connection->InsertQuery($sql, $params);

			return $team_id;
		}

	}

	
	}
	?>