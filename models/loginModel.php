<?php 
class loginModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function authenticate($username, $password, $rem_me)
	{
		$sql = "SELECT * FROM login WHERE email = :username AND password = :password";
		$param = array('username'=>$username, 'password'=>$password);
		$user = $this->connection->Query($sql, $param);

		if($user)
		{
			Session::createNewSession($user,$rem_me);
			return true;
		}
		else
		{
			return false;
		}

	}

	public function forgotPassword($user_id)
	{
		if($user_id)
		{
			$sql = "SELECT user_password_id FROM user_password WHERE user_id = :id";
			$params = array('id' => $user_id);
			$user_password = $this->connection->Query($sql, $params);

			if(!$user_password)
			{
				$sql = "INSERT INTO user_password(user_id) VALUES (:id)";
				$params = array('id' => $user_id );
				$this->connection->InsertQuery($sql, $params);
			}
			
			return true;
		}
		else
		{
			$error = "No such user found";
			return false;
		}
	}

	public function sendEmail($email)
	{
		$to = ltrim($email); // this is your Email address
	    $from = "rashmi@manaram.technology.com"; // this is the sender's Email address
	    $subject = "Set your password";
	    $html = "<a href=" .URL."password/setPassword/". md5($email).">Set Password</a>";
	    $message = "Goto the following link to create your password \n" . $html;

	    


	    if(mail($to,$subject,$message, $from))
	    {
	    	$success_msg = "The mail was successfully sent";
	    	$_POST = array();
	    }
	    else
	    {

	    	$error_msg = "email was NOT sent";
	    	$_POST = array();
	    }

	}
}
?>