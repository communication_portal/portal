$(document).ready(function(){
	$(".error").hide();
	$("#clear").hide();

	$("#clear").click(function () {
		$("#output").html("");
		$('#imageInput').val('');
		$("#clear").hide();
		$('#hidden_photo').val('');

		$("#output").empty();
		return false;
	});

	$('#imageInput').change(function(){
		if(beforeSubmit() != false)
		{
			uploadPhoto(this.files[0]);
			$("#clear").show();
		}
		else
		{
			return false;
		}
	});

	$(".eta-form_accept").submit(function(e){
		$(".error").hide();
		var lot_no = $(this).parent().parent().parent().find('#lot_no').val();
		if(lot_no =="")
		{
			$(this).parent().parent().parent().find("#error_lot_no").show();
			return false;
		}
		else
		{
			return true;
		}
	});

	$("#user_form").submit(function(e){
		$(".error").hide();
		var output = checkUserValidation();
		if (!output)
		{
			return false;
		}
	});

	$("#report_form").submit(function(){
		$(".error").hide();
		var output = checkReportValidation();
		if (!output)
		{
			return false;
		}
	});

	$("#items_form").submit(function(e){
		$(".error").hide();
		var output = checkItemsValidation();

		if (!output)
		{
			return false;
		}
	});

	$(document).on('submit', ".items_dept_form", function(e){
		$(".error").hide();
		var output = checkParlistValidation($(this));

		if (!output)
		{
			return false;
		}
	});

	$(document).on('submit', ".usage-table", function(e){
		$(".error").hide();
		var output = checkDailyUsageValidation($(this));
		if (!output)
		{
			return false;
		}
	});


	

	$("#vendor_form").submit(function(e){
		$(".error").hide();
		var output = checkVendorValidation();
		if (!output)
		{
			return false;
		}

	});
	
	
	$("#department_form").submit(function(e){
		$(".error").hide();
		var output = checkDepartmentValidation();
		if (!output)
		{
			return false;
		}

	});

	$("#warehouse_form").submit(function(e){
		$(".error").hide();
		var output = checkWarehouseValidation();
		if (!output)
		{
			return false;
		}

	});
	
});



function afterSuccess(imageData)
{
	$("#output").append('<input type="hidden" name="photo" id="hidden_photo" /><img src="' + imageData + '" id= "item_img"/>');
	var image_path = $("#item_img").attr('src');

	var image_name = image_path.substr(image_path.lastIndexOf('/') + 1);
	$('#hidden_photo').val(image_name);
  $('#submit-btn').show(); //hide submit button
  $('#loading-img').hide(); //hide submit button

}

function beforeSubmit(){
	if (window.File && window.FileReader && window.FileList && window.Blob)
	{

    if( !$('#imageInput').val()) //check empty input filed
    {
    	$("#output").html("Please insert a photo");
    	$('#imageInput').val('');
    	return false;
    }
    
    var fsize = $('#imageInput')[0].files[0].size; //get file size

    var ftype = $('#imageInput')[0].files[0].type; // get file type
    

    //allow only valid image file types 
    switch(ftype)
    {
    	case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
    	break;
    	default:
    	$("#output").html("Unsupported file type! Please insert Photo");
    	$('#imageInput').val('');
    	return false;
    }
    
    if(fsize>3048576) 
    {
    	$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
    	$('#imageInput').val('');
    	return false;
    }

    $('#submit-btn').hide(); //hide submit button
    $('#loading-img').show(); //hide submit button
    $("#output").html("");  
}
else
{
	$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
	$('#imageInput').val('');
	return false;
}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Bytes';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}


function checkWarehouseValidation()
{
	var returnValue = true;
	if($.trim($("#warehouse_name").val())=="")
	{
		$("#error_name").show();
		returnValue = false;
	}
	// if($("#warehouse_manager").val()==null)
	// {
	// 	$("#error_manager").show();
	// 	returnValue = false;
	// }

	return returnValue;
}

function checkReportValidation()
{
	var returnValue = true;
	if($("#category").val()==null)
	{
		$("#error_category").show();
		returnValue = false;
	}
	if($("#date_from").val()!="" && $("#date_to").val()=="" && $("#period").val()=="")
	{
		$("#error_date").show();
		returnValue = false;
	}
	if($("#period").val()=="")
	{
		if($("#date_from").val()=="" && $("#date_to").val()=="")
		{
			$("#error_period").show();
			returnValue = false;
		}
	}
	return returnValue;
}

function checkDepartmentValidation()
{
	var returnValue = true;
	if($.trim($("#department_name").val())=="")
	{
		$("#error_name").show();
		returnValue = false;
	}
	if($("#user").val()==null)
	{
		$("#error_manager").show();
		returnValue = false;
	}

	return returnValue;
}

function checkVendorValidation()
{
	var returnValue = true;
	if($.trim($("#vendor_name").val())=="")
	{
		$("#error_name").show();
		returnValue = false;
	}
	// if($.trim($("#vendor_contact_person").val())=="")
	// {
	// 	$("#error_contact_person").show();
	// 	returnValue = false;
	// }
	if($.trim($("#vendor_email").val())!="")
	{
		var val = validateEmail($("#vendor_email").val());
		if(val == false)
		{
			$("#error_email_valid").show();
			returnValue = val;
		}
	}
	if($.trim($("#vendor_contact").val())=="")
	{
		// $("#error_contact").show();
		// returnValue = false;
	}
	else
	{
		var valPhone = validatePhone($.trim($("#vendor_contact").val()));
		if(valPhone == false)
		{
			$("#error_valid_contact").show();
			returnValue = valWeb;
		}

	}
	if($.trim($("#vendor_website").val())!="")
	{
		var valWeb = validateWebsite($("#vendor_website").val());
		if(valWeb == false)
		{
			$("#error_website_valid").show();
			returnValue = valWeb;
		}
	}

	return returnValue;
}

function checkItemsValidation()
{
	var returnValue = true ;
	if($.trim($("#item_name").val())=="")
	{
		$("#error_name").show();
		returnValue = false;
	}
	if($("#primary_vendor").val()==null)
	{
		$("#error_primary_vendor").show();
		returnValue = false;
	}
	// if($("#secondary_vendor").val()==null)
	// {
	// 	$("#error_secondary_vendor").show();
	// 	returnValue = false;
	// }
	// alert($("#warehouse").val());
	// returnValue = false;

	if($("#warehouse").val()=="0" || $("#warehouse").val() == null)
	{
		$("#error_warehouse").show();
		returnValue = false;
	}
	if(!$.isNumeric($("#min_warehouse").val()) || $("#min_warehouse").val() == '' || $("#min_warehouse").val() < 0)
	{
		$("#error_min_warehouse").show();
		returnValue = false;
	}
	if(!$.isNumeric($("#max_warehouse").val()) || $("#max_warehouse").val() == '' || $("#max_warehouse").val() < 0 || ($("#max_warehouse").val() - $("#min_warehouse").val()) < 0)
	{
		$("#error_max_warehouse").show();
		returnValue = false;
	}
	// if(!$.isNumeric($("#current_warehouse").val()) || $("#current_warehouse").val() == '' )
	// {
	// 	$("#error_current_warehouse").show();
	// 	returnValue = false;
	// }
	if($("#department").val()==null)
	{
		$("#error_department").show();
		returnValue = false;
	}
	
	return returnValue;
}

function checkParlistValidation(value)
{
	var returnValue = true;
	value.find('tbody tr').each(function(){
		var min = $(this).find('.min_department').val();
		var max = $(this).find('.max_department').val();
		if(min == '' || min < 0)
		{
			$(this).find('.error_min_department').show();
			returnValue = false;
		}
		if(max == '' || max < 0 || (max - min) < 0)
		{
			$(this).find('.error_max_department').show();
			returnValue = false;
		}
	});
	return returnValue;
}

function checkDailyUsageValidation(value)
{
	var returnValue = true;
	value.find('tbody tr').each(function(){
		var current = parseFloat($(this).find('.current').val());
		var usage = parseFloat($(this).find('.usage').val());
		var actual_current = parseFloat($(this).find('.actual_current').val());
		//alert(actual_current);
		//return returnValue;
		
		if(usage < 0 )
		{
			$(this).find('.error_usage').show();
			returnValue = false;
		}
		
		if(usage > actual_current)
		{
			
			$(this).find('.error_usage_current').show();
			returnValue = false;
		}
		// else if(usage > actual_current)
		// {
		// 	alert(1);
		// 	$(this).find('.error_usage_current').show();
		// 	returnValue = false;
		// }




		// else if(usage < current)
		// {
		// 	console.log(current);
		// 	console.log(usage);
		// 	returnValue = false;
		// }
		// else
		// {
		// 	returnValue = false;
		// }
	});
	return returnValue;
}

function checkUserValidation()
{
	var name = $.trim($("#user_name").val());
	var returnValue = true;
	if($("#user_email").val()=="")
	{
		$("#error_email").show();
		returnValue = false;
	}
	else 
	{
		var val = validateEmail($.trim($("#user_email").val()));
		if(val == false)
		{
			$("#error_email_valid").show();
		}
		returnValue = val;

	}
	if(name =="")
	{
		$("#error_name").show();
		returnValue = false;
	}
	else 
	{
		var val = validateName(name);
		if(val == false)
		{
			$("#error_valid_name").show();
		}
		returnValue = val;

	}
	
	// if($("#user_phone").val()=="")
	// {
	// 	$("#error_phone").show();
	// 	returnValue = false;
	// }
	if($("#user_phone").val()=="")
	{
		$("#error_phone").show();
		returnValue = false;
	}
	else
	{
		var valPhone = validatePhone($.trim($("#user_phone").val()));
		if(valPhone == false)
		{
			$("#error_phone").show();
			returnValue = valPhone;
		}

	}

	if($('#user_access_level').val() == '0')
	{
		$("#error_access_level").show();
		returnValue = false;
	}

	return returnValue;
}

function validateEmail(email) {
	var re = /\S+@\S+\.\S+/;
	return re.test(email);
}

function validateWebsite(website)
{
	var re = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,10}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	return re.test(website);
}

function validatePhone(phone)
{
	var re = /^[+]{0,1}[1]{0,1}[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
	var regex = /^[+]{0,1}[1]{0,1}[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[+\s\.]{0,1}[0-9]{2}$/;
	result = regex.test(phone) || re.test(phone);
	return result;
}

function validateName(name)
{
	var re = /^[a-zA-Z\s]+$/;
	return re.test(name);
}