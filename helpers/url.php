<?php

if(!function_exists('redirect'))
{
	function redirect($urlpath = '')
	{
		header("Location: ".URL.$urlpath);
	}
}


if(!function_exists('CurrentURL'))
{
	function CurrentURL() {
	return 'http://'.SERVER_NAME.$_SERVER['REQUEST_URI'];
	}
}

if(!function_exists('redirectToPrevPage'))
{
	function redirectToPrevPage($urlpath = '')
	{
		header("Location: ".$_SERVER['HTTP_REFERER']);
	}
}